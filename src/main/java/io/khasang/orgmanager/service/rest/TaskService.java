package io.khasang.orgmanager.service.rest;


import io.khasang.orgmanager.dao.ITaskDao;
import io.khasang.orgmanager.dao.ITaskListDao;
import io.khasang.orgmanager.dao.IUserDao;
import io.khasang.orgmanager.model.Entities.Task;
import io.khasang.orgmanager.model.Entities.TaskList;
import io.khasang.orgmanager.model.Entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;


@RestController
@CrossOrigin(origins = "*")
public class TaskService {
    @Autowired
    ITaskDao taskDao;

    @Autowired
    ITaskListDao taskListDao;

    @Autowired
    IUserDao userDao;

    private  User getCurrentUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); //get logged in username
        return userDao.getUserByName(name);
    }

    @RequestMapping( value = "/rest/task/{id}", method = RequestMethod.GET)
    public Task getTask(@PathVariable("id") Integer id){
       return taskDao.get(id);
    }

    @RequestMapping( value = "/rest/tasks", method = RequestMethod.GET)
    public List<Task> getUsers() {

        return taskDao.getRelatedToUser(getCurrentUser());
    }

    @RequestMapping(value = "/rest/tasks", method = RequestMethod.POST)
    public ResponseEntity<String> createEmployee(@RequestBody Task task)
    {
        System.out.println(task);
        task.setCreator(getCurrentUser());
        taskDao.save(task);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/rest/task/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Task> updateEmployee(@PathVariable("id") int id, @RequestBody Task task)
    {
        System.out.println(id);
        System.out.println(task);
        taskDao.save(task);
        return new ResponseEntity<Task>(task, HttpStatus.OK);
    }


    @RequestMapping(value = "/rest/task/moveto/{taskid}/{listid}", method = RequestMethod.GET)
    public ResponseEntity<Task> moveToList(@PathVariable("taskid") int taskid, @PathVariable("listid") int listid)
    {
        TaskList taskList=taskListDao.get(listid);
        Task task=taskDao.get(taskid);
        task.setList(taskList);
        taskDao.save(task);
        return new ResponseEntity<Task>(task, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/task/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> updateEmployee(@PathVariable("id") int id)
    {
        System.out.println(id);
        taskDao.delete(taskDao.get(id));
        return new ResponseEntity(HttpStatus.OK);
    }
}
