package io.khasang.orgmanager.dao.impl;

import io.khasang.orgmanager.dao.ITaskListDao;
import io.khasang.orgmanager.model.Entities.Task;
import io.khasang.orgmanager.model.Entities.TaskList;
import io.khasang.orgmanager.model.Entities.User;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;
@Repository
public class TaskListDao extends GenericDaoImpl<TaskList> implements ITaskListDao {
    public TaskListDao() {
        super(TaskList.class);
    }

    @Override
    public List<TaskList> getRelatedToUser(User user) {
        return getSession().createCriteria(TaskList.class)
                .setFetchMode("tasks", FetchMode.JOIN)
                .add( Restrictions.eq("owner", user))
                .list();
    }
}
