var app = angular.module('myapp');

app.controller("task", function ($scope, TaskListFactory, TaskFactory) {
    $scope.addform=[];

    $scope.models = {
        selected: null,
        lists: {}
    };

    var tasks = TaskListFactory.query();

    tasks.$promise.then(
        function (data) {
            for (var i = 0; i < data.length; i++) {
                console.log(data[i].name);
                $scope.models.lists[data[i].name]=data[i].tasks;
                $scope.models.lists[data[i].name].id=data[i].id;
            }
        });

    $scope.dropCallback = function(item, listName, list) {
       TaskFactory.move({taskid: item.id, listid: list.id});
       return item;
    };

    $scope.addto=function (listid) {
        alert(listid);
        $scope.addform[listid]=true;

    }

});

	